terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "~> 2.8.0"
    }
  }
}

provider "proxmox" {
  pm_api_url      = "https://berserk.lan.posttenebraslab.ch:8006/api2/json"
  pm_tls_insecure = true
}

resource "proxmox_lxc" "basic" {
  hostname    = "daisuke"
  target_node = "berserk"
  #ostemplate  = "local:vztmpl/k3os-rootfs-amd64.tar.gz"
  ostemplate   = "local:vztmpl/nixos-20-09-default_133244716.tar.xz"
  unprivileged = false
  onboot       = true
  protection   = false
  start        = true
  swap         = 0
  rootfs {
    storage = "local-zfs"
    size    = "8G"
  }
}
